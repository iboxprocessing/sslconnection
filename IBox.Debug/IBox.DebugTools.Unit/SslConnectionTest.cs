﻿using Xunit;

namespace IBox.DebugTools.Unit
{
    public class SslConnectionTest
    {
        /// <summary>
        /// Позитивный тест подключения к сервису IIS по протоколу SSL
        /// </summary>
        [Theory(DisplayName = "SslServer Connection Positive Test Case")]
        [InlineData("")]
        public void PositiveSslConectionTest(string certificate)
        {
            
        }
        /// <summary>
        /// Негативный тест подключения к сервису IIS по протоколу SSL
        /// </summary>
        [Theory(DisplayName = "SslServer Connection Negative Test Case")]
        [InlineData("")]
        public void NegativeSslConectionTest(string certificate)
        {

        }
    }
}
