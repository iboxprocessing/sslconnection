﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml.Serialization;
using IBox.Debug.WebApp.Contracts;
using System.IO;
using System.Xml;

namespace IBox.Debug.WebApp.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Test controller(implement api connect with ssl)
    /// </summary>
    public sealed class ConnectionController : ApiController
    {        
        /// <summary>
        /// GET api/&lt;controller&gt;
        /// </summary>
        /// <returns></returns>
        public async Task<HttpResponseMessage> Get()
        {
            return await Task.Run(() => new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("this is test response message")
            }).ConfigureAwait(false);
        }

        /// <summary>
        /// POST api/&lt;controller&gt;
        /// </summary>
        public async Task<HttpResponseMessage> Post()
        {
           var request = Request;
           return await Task.Run(async () =>
            {
                var result = new HttpResponseMessage();
                var requestString = await request.Content.ReadAsStreamAsync();
                if (requestString != null)
                {
                    var serilizer = new Serilizer<CommandApi>();
                    // request
                    CommandApi commandRequest = null;
                    using (var ms = new MemoryStream())
                    {
                        requestString.CopyTo(ms);
                        ms.Flush();
                        ms.Position = 0;
                        commandRequest = (CommandApi)serilizer.Desreilize(ms);
                    }                    
                    // response
                    var document = new XmlDocument();
                    using (var memory = new MemoryStream())
                    {
                        var ser = new XmlSerializer(typeof(CommandApi));
                        ser.Serialize(memory,commandRequest);
                        await memory.FlushAsync();
                        memory.Position = 0;
                        document.Load(memory);
                    }
                    result.StatusCode = HttpStatusCode.OK;
                    // test return new instance of CommandApi
                    result.Content = new StringContent(document.InnerXml);
                }
                else
                {
                    result.StatusCode = HttpStatusCode.BadRequest;
                    result.Content = new StringContent("command is empty!");
                }
                return result;
            }).ConfigureAwait(false);
        }
    }
}