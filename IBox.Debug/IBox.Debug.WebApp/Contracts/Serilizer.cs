﻿using System.IO;
using System.Xml.Serialization;

namespace IBox.Debug.WebApp.Contracts
{
    /// <summary>
    /// Сериализация/дсериализация ответов API
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class Serilizer<T> where T : class
    {
        /// <summary>
        /// xml serilialization of object
        /// </summary>
        /// <param name="input"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public Stream Serilize(T input, object value)
        {
            var serilizer = new XmlSerializer(typeof(T));
            var memory = new MemoryStream();
            serilizer.Serialize(memory, value);
            memory.Flush();
            memory.Position = 0;
            return memory;
        }

        /// <summary>
        /// xml deserilization of object
        /// </summary>
        public object Desreilize(Stream input)
        {
            var ser = new XmlSerializer(typeof(CommandApi));
            var result = (T)ser.Deserialize(input);
            return result;
        }
    }
}