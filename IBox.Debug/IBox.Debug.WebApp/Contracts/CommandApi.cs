﻿using System.Xml.Serialization;

namespace IBox.Debug.WebApp.Contracts
{
    [XmlRoot(ElementName = "command")]
    public class CommandApi
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "value")]
        public string Value { get; set; }
    }
}